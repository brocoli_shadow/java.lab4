package lib;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameCharacterTest {

	@Test
	public void testDefaultConstructorAndGetMethods() {
		GameCharacter gc = new GameCharacter();
		assertEquals("Lives field should be initialised with a value of 5", 5, gc.getLivesRemaining());
		assertEquals("Rewards field should be initialised with a value of 0", 0, gc.getRewards());
		assertEquals("RewardsPerLife field should be initialised with a value of 3", 3, gc.getRewardsPerLife());
	}

	@Test
	public void testCustomConstructor() {
		GameCharacter gc = new GameCharacter(2, 1, 5);
		assertEquals("Lives field should be initialised with a value of 2", 2, gc.getLivesRemaining());
		assertEquals("Rewards field should    be initialised with a value of 1", 1, gc.getRewards());
		assertEquals("RewardsPerLife field should be initialised with a value of 5", 5, gc.getRewardsPerLife());
	}

	@Test
	public void testAddRewards1() {
		GameCharacter gc = new GameCharacter();
		gc.addRewards(2);
		assertEquals("Rewards field should be updated with a value of 2", 2, gc.getRewards());
	}

	@Test
	public void testAddRewards2() {
		GameCharacter gc = new GameCharacter(0, 1, 3);
		gc.addRewards(2);
		assertEquals("Rewards field should not be be updated expected valueof 1", 1, gc.getRewards());
	}

	@Test
	public void testSetRewardsPerLife() {
		GameCharacter gc = new GameCharacter();
		gc.setRewardsPerLife(2);
		assertEquals("RewardsPerLife field should be initialised with a value of 2", 2, gc.getRewardsPerLife());
	}

	@Test
	public void testLoseLife1() {
		GameCharacter gc = new GameCharacter();
		gc.loseLife();
		assertEquals("Lives field should be updated with a value of 4", 4, gc.getLivesRemaining());
	}

	@Test
	public void testLoseLife2() {
		GameCharacter gc = new GameCharacter(0, 1, 3);
		gc.loseLife();
		assertEquals("Lives field should not updated and keep value of 0", 0, gc.getLivesRemaining());
	}

	@Test
	public void testBuyLife1() {
		GameCharacter gc = new GameCharacter(1, 3, 2);
		gc.buyLife();
		assertEquals("Lives field should be updated with a value of 2", 2, gc.getLivesRemaining());
		assertEquals("Rewards field should  be updated to the value of 1", 1, gc.getRewards());
	}
	
	@Test
	public void testBuyLife2() {
		GameCharacter gc = new GameCharacter(0, 3, 2);
		gc.buyLife();
		assertEquals("Lives field should not be updated and keep a value of 0", 0, gc.getLivesRemaining());
		assertEquals("Rewards field should  not be updated and keep a value of 3", 3, gc.getRewards());
	}
	
	@Test
	public void testBuyLife3() {
		GameCharacter gc = new GameCharacter(1, 3, 4);
		gc.buyLife();
		assertEquals("Lives field should not be updated and keep a value of 1", 1, gc.getLivesRemaining());
		assertEquals("Rewards field should  not be updated and keep a value of 3", 3, gc.getRewards());
	}

	@Test
	public void testIsAlive1() {
		GameCharacter gc = new GameCharacter(0, 3, 2);
		assertFalse("the isAlive method should return false for lives =0", gc.isAlive());
	}
	
	@Test
	public void testIsAlive2() {
		GameCharacter gc = new GameCharacter(1, 3, 2);
		assertTrue("the isAlive method should return True for lives =1", gc.isAlive());
	}
	
	@Test
	public void testToString(){
		GameCharacter gc = new GameCharacter(0, 3, 2);
		String toStr = gc.toString();
		assertTrue("The toString method should be in the standard convention format",
				toStr.startsWith("GameCharacter[") &&
				toStr.contains("lives=" + gc.getLivesRemaining() + "," )&&
				toStr.contains("rewards= " + gc.getRewards() + ",") &&
				toStr.endsWith("rewardsPerLife= " + gc.getRewardsPerLife() + "]"));
						
}
}