package lib;

import static org.junit.Assert.*;

import org.junit.Test;

public class RectangleTest {

	@Test
	public void testDefaultConstructor() {
		Rectangle r = new Rectangle();
		assertEquals("x parameter should be initialised to 0", 0, r.getX());
		assertEquals("y parameter should be initialised to 0", 0, r.getY());
		assertEquals("width parameter should be initialised to 0", 0, r.getW());
		assertEquals("height parameter should be initialised to 0", 0, r.getH());
		}

	@Test
	public void testCustomtConstructor() {
		Rectangle r = new Rectangle(3, 2, 4, 5);
		assertEquals("x parameter should be initialised to 3", 3, r.getX());
		assertEquals("y parameter should be initialised to 2", 2, r.getY());
		assertEquals("width parameter should be initialised to 4", 4, r.getW());
		assertEquals("height parameter should be initialised to 5", 5, r.getH());
		}

	@Test
	public void testSetAndGetX() {
		Rectangle r = new Rectangle();
		r.setX(7);
		assertEquals("x parameter should be initialised to 7", 7, r.getX());
	}

	@Test
	public void testSetAndGetY() {
		Rectangle r = new Rectangle();
		r.setY(9);
		assertEquals("y parameter should be initialised to 9", 9, r.getY());
	}
	@Test
	public void testSetAndGetW() {
		Rectangle r = new Rectangle();
		r.setW(4);
		assertEquals("width parameter should be initialised to 4", 4, r.getW());
	}

	@Test
	public void testSetAndGetH() {
		Rectangle r = new Rectangle();
		r.setH(1);
		assertEquals("height parameter should be initialised to 1", 1, r.getH());
	}


	@Test
	public void testGetArea() {
		Rectangle r = new Rectangle(1,2,3,4);
		assertEquals("Area should be processed to 12", 12, r.getArea()); 
	}

	@Test
	public void testGetPerimeter() {
		Rectangle r = new Rectangle(1,2,3,4);
		assertEquals("Perimeter should be processed to 14", 14, r.getPerimeter()); 
	}

	@Test
	public void testMove() {
		Rectangle r = new Rectangle();
		r.move(2,3);
		assertEquals("x parameter should be updated to 2", 2, r.getX());
		assertEquals("y parameter should be updated to 3", 3, r.getY());
	}

	@Test
	public void testExpand() {
		Rectangle r = new Rectangle(0,0,1,2);
		r.expand(3, 4);
		assertEquals("width parameter should be updated to 3", 3, r.getW());
		assertEquals("height parameter should be initialised to 8", 8, r.getH()); 
	}

	@Test
	public void testToString() {
		Rectangle r = new Rectangle(1, 2, 3, 4);
		String toStr = r.toString();
		assertTrue("The toString method should be in the standard convention format",
				toStr.startsWith("Rectangle[") && toStr.contains("x=" + r.getX() + " ,")
				&& toStr.contains("y=" + r.getY() + " ,") && toStr.contains("w=" + r.getW() + " ,")
				&& toStr.endsWith("h=" + r.getH() + " ]"));
	}
}
