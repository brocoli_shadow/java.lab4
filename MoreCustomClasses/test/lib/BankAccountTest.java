package lib;

import static org.junit.Assert.*;

import org.junit.Test;

public class BankAccountTest {

	@Test
	public void testdefaultConstructor() {
		BankAccount ba = new BankAccount();
		assertEquals("The balance should be initialised to 0", 0, ba.getBalance());

	}

	@Test
	public void testDefaultconstructor() {
		BankAccount ba = new BankAccount("1234", 50);
		assertEquals("The balance should be initialised to 50", 50, ba.getBalance());
	}

	@Test
	public void testDeposit() {
		BankAccount ba = new BankAccount();
		ba.deposit(24);
		assertEquals("The balance should be updated to 25", 24, ba.getBalance());
	}

	@Test
	public void testWithdraw1() {
		BankAccount ba = new BankAccount("1234", 100);
		ba.withdraw(25);
		assertEquals("The balance should be updated to 75", 75, ba.getBalance());
	}

	@Test
	public void testWithdraw2() {
		BankAccount ba = new BankAccount("1234", 100);
		ba.withdraw(125);
		assertEquals("The balance should not be updated as amount is greater than balance", 100, ba.getBalance());
	}

	@Test
	public void testGetBalance() {
		BankAccount ba = new BankAccount("1234", 100);
		assertEquals("The balance should be returned", 100, ba.getBalance());
	}

}
