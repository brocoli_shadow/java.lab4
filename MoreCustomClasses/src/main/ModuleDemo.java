package main;

import lib.Module;

public class ModuleDemo {

	public static void main(String[] args) {

		System.out.println("Demonstrating some core functionality of the Module...\n======");

		System.out.println("\nUsing default constructor initialisation and get method...");
		Module m1 = new Module();

		System.out.println(m1);
		System.out.println("Expected code: CTEC0000 / Actual code:" + m1.getCode());
		System.out.println("Expected name:  / Actual name:" + m1.getName());
		System.out.println("Expected examweight: 50 / Actual examweight:" + m1.getExamWeight());
		System.out.println("Expected cwkweight: 50 / Actual cwkweight:" + m1.getCwkWeight());

		System.out.println("\nUsing custom constructor with 2 arguments initialisation ...");
		Module m2 = new Module("CTEC1111", "Window sweeping");

		System.out.println(m2);
		System.out.println("Expected code: CTEC1111 / Actual code:" + m2.getCode());
		System.out.println("Expected name: window sweeping / Actual name:" + m2.getName());
		System.out.println("Expected examweight: 50 / Actual examweight:" + m2.getExamWeight());
		System.out.println("Expected cwkweight: 50 / Actual cwkweight:" + m2.getCwkWeight());

		System.out.println("\nUsing custom constructor with 2 arguments initialisation ...");
		Module m3 = new Module("CTEC2222", "CPR", 30);

		System.out.println(m3);
		System.out.println("Expected code: CTEC2222 / Actual code:" + m3.getCode());
		System.out.println("Expected name: CPR / Actual name:" + m3.getName());
		System.out.println("Expected examweight: 30 / Actual examweight:" + (m3.getExamWeight()));
		System.out.println("Expected cwkweight: 70 / Actual cwkweight:" + m3.getCwkWeight());

		System.out.println("\nUsing set methods ...");
		m1.setCode("CTEC1234");
		m1.setName("Underwater crochet");
		m1.setExamWeight(10);
		m1.setCwkWeight(45);
		System.out.println("Expected code: CTEC1234/ Actual code:" + m1.getCode());
		System.out.println("Expected name: Underwater Crochet / Actual name:" + m1.getName());
		System.out.println("Expected examweight: 55 / Actual examweight:" + m1.getExamWeight());
		System.out.println("Expected cwkweight: 45 / Actual cwkweight:" + m1.getCwkWeight());

		System.out.println("\nUsing the tostring method ...");
		System.out.println("Module:[=CTEC1234, =Underwater crochet, =55, =45]/ Actual representation:" + m1.toString());
	}

}
