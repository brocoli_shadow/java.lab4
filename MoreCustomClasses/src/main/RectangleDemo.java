package main;


import lib.Rectangle;

public class RectangleDemo {

	public static void main(String[] args) {
		
		System.out.println("Demonstrating some core functionality of the Rectangle Class...\n======");

		System.out.println("\nUsing default constructor initialisation and get methods...");
		Rectangle r1 = new Rectangle();
		System.out.println(r1);
		System.out.println("Expected x: 0 / Actual x:" + r1.getX());
		System.out.println("Expected y: 0 / Actual y:" + r1.getY());
		System.out.println("Expected w: 0 / Actual w:" + r1.getW());
		System.out.println("Expected h: 0 / Actual h:" + r1.getH());
		
		System.out.println("\nUsing custom constructor  initialisation ...");
		Rectangle r2 = new Rectangle(1, 2, 5, 10);
		System.out.println(r2);
		System.out.println("Expected x: 1 / Actual x:" + r2.getX());
		System.out.println("Expected y: 2 / Actual y:" + r2.getY());
		System.out.println("Expected w: 5 / Actual w:" + r2.getW());
		System.out.println("Expected h: 10 / Actual h:" + r2.getH());
		
		System.out.println("\nUsing set and get methods ...");
		r1.setX(4);
		r1.setY(5);
		r1.setW(6);
		r1.setH(7);
		System.out.println("Expected x: 4 / Actual x:" + r1.getX());
		System.out.println("Expected y: 5 / Actual y:" + r1.getY());
		System.out.println("Expected w: 6 / Actual w:" + r1.getW());
		System.out.println("Expected h: 7 / Actual h:" + r1.getH());
		
		System.out.println("\nUsing the get perimeter method");
		System.out.println("Expected perimeter: 26 / Actual x:" + r1.getPerimeter());
		
		System.out.println("\nUsing the get area method");
		System.out.println("Expected area: 42 / Actual x:" + r1.getArea());
		
		System.out.println("\nUsing the move method ...");
		r1.move(3, 2);
		System.out.println("Expected x: 7 / Actual x:" + r1.getX());
		System.out.println("Expected y: 7 / Actual y:" + r1.getY());
		
		System.out.println("\nUsing the expand method ...");
		r1.expand(2, 3);
		System.out.println("Expected w: 12 / Actual w:" + r1.getW());
		System.out.println("Expected h: 21 / Actual h:" + r1.getH());
		
		System.out.println("\nUsing the toString method ...");
		Rectangle rec = new Rectangle(-2,1,5,6);
		System.out.println("Expected String:Rectangle[x=-2 ,y=1 ,w=5 ,h=6 ] / Expected String:" + rec.toString());
		
		System.out.println("\nUsing the printRectangleGrid method ...");
		rec.printRectangleGrid('-', 12, 12);
	}
	}


