package main;

import lib.OrderLine;

public class OrderLineApp {

	public OrderLineApp() {
		
	}

	public static void main(String[] args) {

		System.out.println("Demonstrating some core functionality of the OrderLine...\n======");

		System.out.println("\nUsing default constructor initialisation and get method...");
		OrderLine o1 = new OrderLine();
		System.out.println(o1);
		System.out.println("Expected id: /  Actual:" + o1.getId());
		System.out.println("Expected Unit Price: 0  / Actual:" + o1.getUnitPrice());
		System.out.println("Expected Quantity:  0 / Actual:" + o1.getQuantity());

		System.out.println("\n Using custom constructor initialisation and get method...");
		OrderLine o2 = new OrderLine("Orange", 20, 10);
		System.out.println(o2);
		System.out.println("Expected id: Orange /  Actual:" + o2.getId());
		System.out.println("Expected Unit Price: 20  / Actual:" + o2.getUnitPrice());
		System.out.println("Expected Quantity:  10 / Actual:" + o2.getQuantity());

		System.out.println("\n Using the set methods...");
		OrderLine o3 = new OrderLine();
		o3.setId("banana");
		o3.setUnitPrice(15);
		o3.setQuantity(4);
		System.out.println("Expected id: Banana /  Actual:" + o3.getId());
		System.out.println("Expected Unit Price: 15  / Actual:" + o3.getUnitPrice());
		System.out.println("Expected Quantity:  4 / Actual:" + o3.getQuantity());

		System.out.println("\n Using the getcost method ...");
		System.out.println("Expected cost: 60 /  Actual:" + o3.getCost());

		System.out.println("\n Using the toString method ...");
		System.out.println("Expected output: [Id=banana, Unit Price=15, Quantity=4] / Actual:" + o3.toString());

		System.out.println("\nUsing the equal method ...");
		OrderLine o4 = new OrderLine("banana", 15, 4);
		System.out.println("Expected assertion: True / Actual: " + o3.equals(o4));
		System.out.println("Expected assertion: False / Actual: " + o1.equals(o4));

	}

}
