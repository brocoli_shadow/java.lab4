package lib;
/**
 * A GameCharacter has lives and can accumulate rewards. Those rewards can be used 
 * to buy lives on the basis of a rewardsPerLife rate. When the character has 0 lives 
 * he can't perform the followings:
 * -buy lives
 * -collect lives
 * -lose lives
 * @author leice
 *
 */
public class GameCharacter {

	// Fields
	
	private int lives;
	private int rewards;
	private int rewardsPerLife;

	// Default Constructor
	/**
	 * Default constructor, initialises lives to 5, rewards to 0 and
	 * rewards per life to 3.
	 */
	
	public GameCharacter() {
		this.lives = 5;
		this.rewards = 0;
		this.rewardsPerLife = 3;
	}

	// custom Constructor
	/**
	 * Custom constructor, initialises a newly created Gamecharacter object
	 * with custom values.
	 * 
	 * @param lives
	 * @param rewards
	 * @param rewardsPerLife
	 */
	public GameCharacter(int lives, int rewards, int rewardsPerLife) {
		this.lives = lives;
		this.rewards = rewards;
		this.rewardsPerLife = rewardsPerLife;
	}

	// Methods
	/**
	 * Increments the current value of the rewards by the given amount
	 * only if object is alive.
	 * 
	 * @param amount The current int value at which the rewards should be increased
	 */
	public void addRewards(int amount) {
		if (this.isAlive() == true) {
			rewards = rewards + amount;
		}
	}
	/**
	 * Sets the value of the rewardsPerLife rate.
	 * 
	 * @param amount the int value at which the rewardsPerLife should be set.
	 */
	public void setRewardsPerLife(int amount) {
		rewardsPerLife = amount;
	}
	/**
	 * Returns the amount of lives held by the GameCharacter.
	 * 
	 * @return The amount of lives held by the GameCharacter.
	 */
	public int getLivesRemaining() {
		return lives;
	}
	/**
	 * Returns the rate at which rewards can be exchanged 
	 * to buy a life.
	 * 
	 * @return the rate at which rewards can be exchanged to buy a life.
	 */
	public int getRewardsPerLife(){
		return rewardsPerLife;
	}
	/**
	 * Returns the number of rewards held by the GameCharacter.
	 * 
	 * @return the number of remaining rewards.
	 */
	public int getRewards() {
		return rewards;
	}
	/**
	 * Decrements the number of lives held by the GameCharacter 
	 * by 1 at the condition that the object is in a alive state.
	 */
	public void loseLife() {
		if (this.isAlive() == true) {
			lives = lives - 1;
		}
	}
	/**
	 * Increments the value of lives by 1 at the condition that
	 * the object is in a alive state.
	 */
	public void buyLife() {
		if (lives > 0 && rewards >= rewardsPerLife) {
			rewards = rewards - rewardsPerLife;
			lives = lives + 1;
		}
	}
	/**
	 * Returns true if the GameCharacter has more than 0 lives.
	 * 
	 * @return true if the object has more than 0 lives otherwise will 
	 * return false
	 */
	public boolean isAlive() {
		if (lives > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Returns a textual representation of the GameCharacter.
	 * 
	 * @return a textual representation of the GameCharacter.
	 */
	@Override
	public String toString(){
		return "GameCharacter[lives=" + this.lives + ",rewards= " + this.rewards +  ",rewardsPerLife= " + this.rewardsPerLife + "]";
	}

}
