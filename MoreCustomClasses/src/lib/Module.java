package lib;

public class Module {

	// Fields
	private String code;
	private String name;
	private int examWeight;
	private int cwkWeight;

	// default constructor
	public Module() {
		code = "CTEC0000";
		name = "";
		examWeight = 50;
		cwkWeight = 50;
	}

	// Custom Constructor with 2 arguments
	public Module(String code, String name) {
		this.code = code;
		this.name = name;
		this.examWeight = 50;
		this.cwkWeight = 50;
	}

	// Custom Constructor with 3 arguments
	public Module(String code, String name, int examWeight) {
		this.code = code;
		this.name = name;
		this.examWeight = examWeight;
		this.cwkWeight = 100 - examWeight;
	}

	// set methods
	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setExamWeight(int examWeight) {
		this.examWeight = examWeight;
		this.cwkWeight = 100 - examWeight;
	}

	public void setCwkWeight(int cwkWeight) {
		this.cwkWeight = cwkWeight;
		this.examWeight = 100 - cwkWeight;
	}

	// get methods

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public int getExamWeight() {
		return examWeight;
	}

	public int getCwkWeight() {
		return cwkWeight;
	}

	// tostring method

	@Override
	public String toString() {
		return "Module:[" + "=" + code + ", " + "=" + name + ", " + "=" + examWeight + ", " + "=" + cwkWeight + "]";

	}

}
