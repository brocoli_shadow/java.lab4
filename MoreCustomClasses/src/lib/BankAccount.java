package lib;

public class BankAccount {

	// Fields
	private String accountNo;
	private int balance;

	// Default Constructor
	public BankAccount() {
		this.accountNo = "";
		this.balance = 0;
	}

	// Custom constructor
	public BankAccount(String accountNo, int balance) {
		this.accountNo = accountNo;
		this.balance = balance;
	}

	// Methods
	public void deposit(int amount) {
		balance = balance + amount;
	}

	public void withdraw(int amount) {

		if (balance >= amount) {
			balance = balance - amount;
		}
	}

	public int getBalance() {
		return balance;
	}
}
