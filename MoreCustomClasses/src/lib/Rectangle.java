package lib;

public class Rectangle {
	
	//Fields
	private int x;
	private int y;
	private int w;
	private int h;
	
	//Default constructor
	public Rectangle(){
		this.x = 0;
		this.y = 0;
		this.w = 0;
		this.h = 0;
	}	
	//Custom constructor
	public Rectangle(int x, int y, int w, int h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
		
	//Set methods
	public void setY(int y){
		this.y = y;
	}
	public void setX(int x){
		this.x = x;
	}
	public void setW(int w){
		this.w = w;
	}
	public void setH(int h){
		this.h = h;
	}
	
	//get methods
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public int getW(){
		return w;
	}
	public int getH(){
		return h;
	}
	public int getArea(){
		return w * h;
	}
	public int getPerimeter(){
		return (2*w) + (2*h);
	}
	
	//moving and transformation methods
	public void move(int dx, int dy){
		this.x = x + dx;
		this.y = y + dy;
	}
	public void expand(double scaleW, double scaleH){
		this.w =(int) Math.round(w * scaleW);
		this.h =(int) Math.round(h * scaleH);
	}
	
	//Tostring method
	@Override
	public String toString(){
		return "Rectangle[x=" + x + " ,y=" + y + " ,w=" + w + " ,h=" + h + " ]";
	}
	
	public void printRectangleGrid(char fill, int width, int height) {
		for (int i = 1; i <= width; i++) {

			for (int j = 1; j <= height; j++) {
				//PRINT GRID
				if (j == 1) {
					System.out.print(i + "\t");
				} else if (i == 1) {
					System.out.print(j + "\t");
				} else {

					//PRINT RECTANGLE
					if ((j >= x && j < (x+w)) && (i >= y && i < (y+h))) {
						System.out.print(fill + "\t");
					} else {
						System.out.print("\t");
					}

				}
			}

			System.out.println("\n");
		}
	
	
}}