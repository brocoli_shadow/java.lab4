package lib;

public class OrderLine {
	
	
	//fields
	
	private String id;
	private int unitPrice;
	private int quantity;
	
	//Constructors
	
	//Default constructor
	
		public OrderLine(){
		id = "";
		unitPrice = 0;
		quantity = 0;
		}

		//Custom Constructor
		public OrderLine (String id, int unitPrice, int quantity){
		this.id = id;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		}
	
		//Set Methods
		//set method for id
		public void setId(String id){
		this.id = id;
		}
	
		//Set Method for unitPrice
		public void setUnitPrice(int unitPrice){
			this.unitPrice = unitPrice;
		}
	
		//set method for quantity
		//Set Methods
		
		public void setQuantity(int quantity){
			this.quantity = quantity;
		}
	
		
		//get methods
		//Get method for id
		public String getId(){
			return id;
		}
		
		//get method for unitPrice
		public int getUnitPrice(){
			return unitPrice;
		}
		
		//get  method for quantity
		public int getQuantity(){
			return quantity;
		}
		
		//get cost method
		public int getCost(){
			return unitPrice * quantity;
		}
		
		//Overriden methods
		//custom toString method
		@Override
		public String toString() {
			return "OrderLine:[Id=" + id + ", Unit Price=" + unitPrice + ", Quantity=" + quantity + "]";
		}
		
		//custom equal method
		public boolean equals(Object obj) {
			// test exceptional cases, i.e. obj not null and is a Name
			if (obj == null || this.getClass() != obj.getClass())
				return false;

			OrderLine other = (OrderLine) obj; // downcast to a Name object

			// compare first & family names using String' .equals() method
			return this.id.equals(other.id)
				&& this.unitPrice==other.unitPrice 
				&& this.quantity == other.quantity;
		}
		
}
